FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
RUN ls -la
ENTRYPOINT ["java","-jar","/app.jar"]
